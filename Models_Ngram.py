from nltk.util import pad_sequence
from nltk.util import bigrams
from nltk.util import ngrams
from nltk.util import everygrams
from nltk.lm.preprocessing import pad_both_ends
from nltk.lm.preprocessing import flatten
from nltk.tokenize.treebank import TreebankWordDetokenizer
from nltk import word_tokenize, sent_tokenize 
from nltk.lm.preprocessing import padded_everygram_pipeline
import sys
import random

'''
text = [['c', 'a', 'c'], ['c', 'a', 'i', 'o', 'e', 'f']]

list(bigrams(text[0]))

list(ngrams(text[1], n=3))

from nltk.util import pad_sequence
list(pad_sequence(text[0],
                  pad_left=True, left_pad_symbol="<s>",
                  pad_right=True, right_pad_symbol="</s>",
                  n=2)) # The n order of n-grams, if it's 2-grams, you pad once, 3-grams pad twice, etc.    
padded_sent = list(pad_sequence(text[0], pad_left=True, left_pad_symbol="<s>", 
                                pad_right=True, right_pad_symbol="</s>", n=2))
list(ngrams(padded_sent, n=2))

list(pad_sequence(text[0],
                  pad_left=True, left_pad_symbol="<s>",
                  pad_right=True, right_pad_symbol="</s>",
                  n=3)) # The n order of n-grams, if it's 2-grams, you pad once, 3-grams pad twice, etc. 
padded_sent = list(pad_sequence(text[0], pad_left=True, left_pad_symbol="<s>", 
                                pad_right=True, right_pad_symbol="</s>", n=3))
list(ngrams(padded_sent, n=3))

from nltk.lm.preprocessing import pad_both_ends
list(pad_both_ends(text[0], n=2))

list(bigrams(pad_both_ends(text[0], n=2)))

from nltk.util import everygrams
padded_bigrams = list(pad_both_ends(text[0], n=2))
list(everygrams(padded_bigrams, max_len=2))

from nltk.lm.preprocessing import flatten
list(flatten(pad_both_ends(sent, n=2) for sent in text))

from nltk.lm.preprocessing import padded_everygram_pipeline
train, vocab = padded_everygram_pipeline(2, text)

training_ngrams, padded_sentences = padded_everygram_pipeline(2, text)
#for ngramlize_sent in training_ngrams:
#   print(list(ngramlize_sent))
#   print()
print('#############')
list(padded_sentences)

try: # Use the default NLTK tokenizer.
    from nltk import word_tokenize, sent_tokenize 
    # Testing whether it works. 
    # Sometimes it doesn't work on some machines because of setup issues.
    word_tokenize(sent_tokenize("This is a foobar sentence. Yes it is.")[0])
except: # Use a naive sentence tokenizer and toktok.
    import re
    from nltk.tokenize import ToktokTokenizer
    # See https://stackoverflow.com/a/25736515/610569
    sent_tokenize = lambda x: re.split(r'(?<=[^A-Z].[.?]) +(?=[A-Z])', x)
    # Use the toktok tokenizer that requires no dependencies.
    toktok = ToktokTokenizer()
    word_tokenize = word_tokenize = toktok.tokenize
'''
import os
import requests
import io #codecs

def defineCorpus(fileText):
    print("Step 1 - Define Corpus")
    # Text version of https://kilgarriff.co.uk/Publications/2005-K-lineer.pdf
    if os.path.isfile(fileText):
        with io.open(fileText, encoding='utf8') as fin:
            text = fin.read()
    else:
        url = "https://gist.githubusercontent.com/alvations/53b01e4076573fea47c6057120bb017a/raw/b01ff96a5f76848450e648f35da6497ca9454e4a/language-never-random.txt"
        text = requests.get(url).content.decode('utf8')
        with io.open('language-never-random.txt', 'w', encoding='utf8') as fout:
            fout.write(text)
    return text    
'''
if os.path.isfile('WikiQA-train.txt'):
    with io.open('WikiQA-train.txt', encoding='utf8') as fin:
        text = fin.read()
else:
    url = "https://gist.githubusercontent.com/alvations/53b01e4076573fea47c6057120bb017a/raw/b01ff96a5f76848450e648f35da6497ca9454e4a/language-never-random.txt"
    text = requests.get(url).content.decode('utf8')
    with io.open('language-never-random.txt', 'w', encoding='utf8') as fout:
        fout.write(text)
'''



##################################### Training an N-gram Model##################################### 
def trainingModel(text):
    print("Step 2 - Training an N-gram Model")
    # Tokenize the text.
    tokenized_text = [list(map(str.lower, word_tokenize(sent))) 
                      for sent in sent_tokenize(text)]

    tokenized_text[0]


    # Preprocess the tokenized text for 3-grams language modelling
    n = 3
    train_data, padded_sents = padded_everygram_pipeline(n, tokenized_text)

    from nltk.lm import MLE
    model = MLE(n) # Lets train a 3-grams model, previously we set n=3
    #len(model.vocab)
    model.fit(train_data, padded_sents)
    print("\tVocabulary Informations:")
    print("\t"+model.vocab)
    len(model.vocab)
    #print(model.vocab.lookup(tokenized_text[0]))
    return model


##################################### Generation using N-gram Language Model ##################################### 
from nltk.tokenize.treebank import TreebankWordDetokenizer

detokenize = TreebankWordDetokenizer().detokenize

def generate_sent(model,num_words,random_seed=42):
    
    content = []
    for token in model.generate(num_words, random_seed=random_seed):
        if token == '<s>':
            continue
        if token == '</s>':
            break
        content.append(token)
    return detokenize(content)

def generationWord(model,num_words, randomNumber):
    print("##################################### Generation using N-gram Language Model ##################################### ")
    print(generate_sent(model, num_words, random_seed=randomNumber))
   


##################################### Saving the model ##################################### 

def saveModel(model, fileText):
    print("Step 4 - Saving the model ")
    import dill as pickle 

    with open(fileText+'.pkl', 'wb') as fout:
        pickle.dump(model, fout)

   



##################################### Run ##################################### 

 
def main():

    fileCorpus = sys.argv[1]
    num_words = sys.argv[2]
    randomNumber = random.randint(7,42)
    text = defineCorpus(fileCorpus)
    model = trainingModel(text)
    
    print("Step 3 - Generation using N-gram Language Model:")
    print("##################################### ")
    generationWord(model,int(num_words), int(randomNumber))
    print("##################################### ")
    saveModel(model, fileCorpus)

if __name__ == "__main__":
    main()