Universidade Federal do Maranhão
Doutorado em Ciência da Computação
Disciplina: Processamento de Linguagem Natural
Professor: Anselmo Cardoso de Paiva
Aluno: Caio Eduardo Falcão Matos

Instruções:

Comando para Execução:

	python Models_Ngram.py [fileCorpusText] [numberOfWords]

[fileCorpusText] 
	Arquivo contendo o corpus a ser processado
[numberOfWords]
	Nº máx. de palavras para gerar.